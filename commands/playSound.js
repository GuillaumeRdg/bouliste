'use strict';

const fs = require('fs');
const Discord = require('discord.js');

module.exports = {
  name: 'sound',
  description: 'play a sound',
  requireClient: true,
  init(client) {
    const soundFilesPath = './assets/sounds';
    const sounds = fs.readdirSync(soundFilesPath).filter(file => file.endsWith('.mp3'));

    for (const soundFile of sounds) {
      const soundName = soundFile.split('.').slice(0, -1).join('.');
      client.sounds.set(soundName, `${soundFilesPath}/${soundFile}`);
    }
  },
  async execute(message, args, client) {
    const soundAsked = args[0];

    if (soundAsked === 'help') {
      let str = `the available sounds are : \n`;
      for (const [soundName, soundFile] of client.sounds) {
        str += `${soundName} \n`
      }
      return message.channel.send(str);
    }

    if (!soundAsked) return message.channel.send('No sound selected!');

    const voiceChannel = message.member.voice.channel;
    if (!voiceChannel) return message.channel.send('You need to be in a voice channel to play sounds!');

    if (!client.sounds.has(soundAsked)) return message.channel.send('This sound does not exist!');

    const soundAskedFile = client.sounds.get(soundAsked);

    const connection = await voiceChannel.join();
    const dispatcher = connection.play(soundAskedFile);
    dispatcher.on('finish', () => connection.disconnect());
  },
};
