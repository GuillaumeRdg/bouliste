'use strict';

module.exports = {
  name: 'shifumi',
  description: 'play shifumi',
  execute(message, args) {
    const list = ['pierre', 'feuille', 'ciseaux'];

    const botChoice = list[Math.floor(Math.random() * list.length)];
    const playerChoice = args[0];

    if (playerChoice.includes('puit')) {
      message.channel.send(`PUITS PUITS PUITS PUITS, oh quelle surprise t'as fais un puits aussi fils de pute, on est encore à égalité bravo`);
      return;
    }

    if (list.indexOf(playerChoice) === -1) {
      message.channel.send(`le ${playerChoice} ne fait pas partie du shifumi. recommence avec pierre, feuille ou ciseaux`);
      return;
    }

    if (playerChoice === botChoice) {
      message.channel.send(`${message.author.username} : ${playerChoice}  // Bot : ${botChoice}`);
      message.channel.send(`Egalité, no fun`);
      return;
    }

    let result = '';

    if (playerChoice === 'pierre' && botChoice === 'ciseaux' ||
      playerChoice === 'ciseaux' && botChoice === 'feuille' ||
      playerChoice === 'feuille' && botChoice === 'pierre') {
      result = 'win GG!';
    } else {
      result = 'lose, gros naze !';
    }

    message.channel.send(`${message.author.username} : ${playerChoice}  // Bot : ${botChoice}`);
    message.channel.send(result);
  },
};
