'use strict';

const fs = require('fs');
const keypress = require('keypress');
const Discord = require('discord.js');
const Client = require('./client/Client');

const client = new Client();
client.commands = new Discord.Collection();
client.sounds = new Discord.Collection();

const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
for (const file of commandFiles) {
  const command = require(`./commands/${file}`);
  client.commands.set(command.name, command);

  if (command.init) command.init(client);

  //TODO ADD A ROLE MANAGER FOR COMMANDS
}

client.once('ready', () => {
  console.info('ready');
  setKeyListener();
});

// DRAFT TO BE UPDATED
/**
 * @name setKeyListener;
 * @description
 * KeyListener to be used as a sounds deck
 */
function setKeyListener() {
  keypress(process.stdin);
  process.stdin.setRawMode(true);
  process.stdin.resume();

  let channel = null;

  const guild = client.guilds.fetch(client.config.guildId)
    .then(guild => {
      guild.members.fetch(client.config.adminUserId)
        .then(user => {
          channel = user.voice.channel;
        })
        .catch(console.error);

    })
    .catch(console.error);

  process.stdin.on('keypress', async(chunk, key) => {
    console.log(key);
    if (key && key.ctrl && key.name === 'c') process.exit();

    if (key.shift && channel) {
      switch (key.name) {
        case 'a':
          try {
            const connection = await channel.join();
            const dispatcher = connection.play('./assets/sounds/honteux.mp3');
            dispatcher.on('finish', end => {
              console.log(end);
              connection.disconnect();
            });
          } catch (err) {
            console.log(err);
          }
          break;
      }
    }
  });
}

//END DRAFT

client.on('message', async message => {
  const prefix = client.config.commandPrefix;

  if (message.author.bot) return;
  if (!message.content.startsWith(prefix)) return;

  const args = message.content.slice(prefix.length).split(/ +/);
  const commandName = args.shift().toLowerCase();
  const command = client.commands.get(commandName);

  try {
    if (command.requireClient) {
      command.execute(message, args, client);
    } else {
      command.execute(message, args);
    }
  } catch (error) {
    console.error(error);
    message.reply('There was an error trying to execute that command!');
  }
});

client.login(client.config.appToken);
