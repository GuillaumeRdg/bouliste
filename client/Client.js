const { Client, Collection } = require('discord.js');
const { appToken, commandPrefix, adminUserId, guildId } = require('./../config.json');

module.exports = class extends Client {
  constructor() {
    super({
      disableEveryone: true,
      disabledEvents: ['TYPING_START'],
    });

    this.commands = new Collection();
    this.sounds = new Collection();

    this.config = {
      appToken,
      commandPrefix,
      adminUserId,
      guildId
    };
  }
};
